# Default values for thehive.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: thehiveproject/thehive4
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext:
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  runAsNonRoot: true
  runAsUser: 1000

service:
  type: NodePort
  nodePort: 30900
  port: 9000

ingress:
  enabled: false
  annotations:
    kubernetes.io/ingress.class: "salient-ingress-class"
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
    #kubernetes.io/ingress.class: "openbanking-ingress-class"
  hosts:
    - host: thehive.salient.f5lab
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: salient-thehive
            port:
              number: 9000
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}

# Elasticsearch can be used for indexing. To avoid using it, set
# elasticsearch.eck.enabled and elasticsearch.external.enabled both to
# false.
elasticsearch:
  # Elasticsearch set up outside this chart using Elastic Cloud on
  # Kubernetes (ECK)
  eck:
    # If true, names of resources (e.g. secrets, services) used will
    # all be formed using the elasticsearchName below.
    enabled: false
    # Specify the name of the Elasticsearch custom resource, which
    # must have been successfully created before using this chart.
    name: ""
  # Elasticsearch set up outside this chart, with less
  # assumptions. You will need to set more values; read on below.
  external:
    enabled: true
  # If ECK is used, you don't need to mess with the rest of these.
  # -----------------------------------------------------------------
  #
  # Username to use when connecting to Elasticsearch.
  username: elastic
  # The name of an opaque secret, whose data is a mapping (dictionary,
  # object) with the username as key and the password as value. If ECK
  # is enabled, the default is the secret created by ECK. If ECK is
  # not enabled, this must be set.
  userSecret: elastic-password-secret
  # The name of the Elasticsearch host to connect to. If ECK is
  # enabled, the ECK service is used. If ECK is not enabled, this must
  # be set.
  hostname: "elasticsearch-master:9200"
  # If true, this chart expects to tell Cortex a CA cert to trust when
  # connecting to Elasticsearch. If ECK is enabled, this has to be set
  # to true. If ECK is not enabled and this is set to true, you need
  # to provide a CA certificate using the caCertSecret or caCert
  # values.
  tls: false
  # The name of a Kubernetes Secret object containing a CA cert to
  # trust when connecting to Elasticsearch using HTTPS. The secret
  # should contain a mapping with a key, named by
  # `caCertSecretMappingKey`, whose value is the PEM-encoded cert. If
  # ECK is enabled, the default is the appropriate secret created by
  # ECK. If ECK is not enabled, and this is not set, a Secret will be
  # created using the `caCert` value below. N.B. despite the
  # juxtaposition of the words `caCert` and `Secret`, the private key
  # of the certificate authority is far from what we are talking about
  # here.
  # caCertSecret: elastic-https-ca-secret-pem
  # The name of the key inside the caCertSecret, whose value is the
  # PEM-encoded cert. N.B. despite the juxtaposition of the words
  # `caCert` and `Secret`, the private key of the certificate
  # authority is far from what we are talking about here.
  # caCertSecretMappingKey: elasticsearch-ca.pem
  # A secret is created and used for CA cert trusting, using a
  # PEM-encoded cert from this value, if ECK is not enabled and
  # caCertSecret is not set.
  # caCert: ""

# If the storage class for a persistent volume claim is unset or
# empty, this storage class will be used.
storageClass:

attachmentStorage:
  pvc:
    enabled: true
    storageClass:
    size: 10Gi

# Local database storage is used if Cassandra is not enabled.
localDatabaseStorage:
  pvc:
    enabled: true
    storageClass:
    size: 10Gi

# Local index storage is used if Elasticsearch is not enabled.
localIndexStorage:
  pvc:
    enabled: true
    storageClass:
    size: 10Gi

cassandra:
  enabled: true
  # Among the many configurable values of the Cassandra chart, this
  # was the first I needed to change.
  persistence:
    storageClass:
  # thehive docker entrypoint assumes this cluster name
  cluster:
    name: thp
    # remainder is defaults from cassandra values
    seedCount: 1
    numTokens: 256
    datacenter: dc1
    rack: rack1
    enableRPC: true
    endpointSnitch: SimpleSnitch
    ## Encryption values. NOTE: They require tlsEncryptionSecretName
    ##
    internodeEncryption: none
    clientEncryption: false
    ## extraSeeds - For an external/second cassandra ring. Seed list will be appended by this.
    ##
    # extraSeeds:
      #   - hostname/IP
      #   - hostname/IP


# Provide extra pieces of TheHive configuration in this map. Keys will
# be used as filenames; values as the contents of the respective
# files. An include directive for each file given here will be written
# in the main configuration file. The file contents will be stored in
# a secret, so it is OK to put secrets like API keys in here. In
# particular, connections to Cortex instances should be configured
# here as extras.
extraHiveConfigurations:
  misp_connector.conf: |
    play.modules.enabled += org.thp.thehive.connector.misp.MispModule
    play.ws.ssl.loose.acceptAnyCertificate = true
    misp {
      interval: 5m
      servers: [
        {
          name = "ORGNAME"
          url = "https://stir-misp-web:8443"
          auth {
            type = key
            key = "JYei836hBtV3pjkez1EvBI7ZDSqMXRE539did61O"
          }
          wsConfig {
            ssl {
              loose {
                acceptAnyCertificate : true
              }
            }
          }
          purpose = ExportOnly
          caseTemplate = "misp"
          tags = ["misp-server-id"]
          max-age = 7 days
          exclusion {
            organisation = ["bad organisation", "other orga"]
            tags = ["tag1", "tag2"]
          }
          whitelist {
            tags = ["tag1", "tag2"]
          }
          includedTheHiveOrganisations = ["*"]
          excludedTheHiveOrganisations = []
      }
      ]
    }
  cortex_connector.conf: |
    ## Cortex configuration
    play.modules.enabled += org.thp.thehive.connector.cortex.CortexModule
    cortex {
      servers = [
        {
          name = Cortex
          url = "http://stir-cortex:9001"
          auth {
            type = "bearer"
            key = "sMPGmBeX1vNMTUdUztUy2VthCQcAlLq3"
          }
          wsConfig {}
        includedTheHiveOrganisations = ["*"]
        excludedTheHiveOrganisations = []
        }
      ]
      refreshDelay = 5 seconds
      maxRetryOnError = 3
      statusCheckInterval = 1 minute
    }
  node-red-webhook.conf: |
    notification.webhook.endpoints = [
      {
        name: node-red
        url: "http://stir-node-red:1880/"
        version: 0
        wsConfig: {}
        auth: {type: "none"}
        includedTheHiveOrganisations: ["*"]
        excludedTheHiveOrganisations: []
      }
    ]

# Secrets containing CA certs to trust when TheHive makes outgoing TLS
# connections. Each Secret named here should contain a key "ca.crt"
# whose value is the PEM-encoded CA certificate.
trustRootCertsInSecrets: []

# CA certs to trust when TheHive makes outgoing TLS connections. This
# chart will create Secrets containing the certs. Each item should be
# the text of a single PEM-encoded certificate.
trustRootCerts: []
